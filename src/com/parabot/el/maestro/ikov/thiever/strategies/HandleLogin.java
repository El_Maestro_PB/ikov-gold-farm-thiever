package com.parabot.el.maestro.ikov.thiever.strategies;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import com.parabot.el.maestro.ikov.thiever.util.Methods;
import com.parabot.el.maestro.ikov.thiever.util.accountmaker.Account;
import org.parabot.core.Context;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by Bautista on 7/14/2015.
 */
public class HandleLogin implements Strategy {

    private Point point = new Point(516, 134);

    @Override
    public boolean activate() {
        return !Game.isLoggedIn() || Game.getOpenBackDialogId() == 4900;
    }

    public void execute() {
        Variables.setStatus("Logging in...");
        if (Game.isLoggedIn() && Game.getOpenBackDialogId() == 4900) {
            Time.sleep(1000);
            org.rev317.min.api.methods.Menu.sendAction(679, 17825792, 113, 4907, 1088, 1);
            Time.sleep(500);
        }
        do {
            if (Variables.getAccountPassword() == null) {
                Variables.setAccountPassword(Methods.getPassword());
            } else if (!Methods.getPassword().equalsIgnoreCase(Variables.getAccountPassword())) {
                Methods.setPassword(Variables.getAccountPassword());
            }
            if (!Methods.isMessageShowing()) {
                Keyboard.getInstance().clickKey(KeyEvent.VK_ENTER);
                Time.sleep(5000);
            } else {
                Variables.setStatus("Closing loggin message...");
                Mouse.getInstance().click(point);
                Time.sleep(1000);
            }
        } while (!Game.isLoggedIn() && !Methods.isBanned());
        if (!Game.isLoggedIn() && Methods.isBanned()) {
            Variables.setStatus("Account has been banned...");
            Variables.setAccountsBanned(Variables.getAccountsBanned() + 1);
            if (Variables.getAccountCreater()) {
                Variables.setStatus("Generating new account...");
                Variables.setUpdateStall(true);
                Variables.setFreshAccount(true);
                final Account account = Variables.getAccountGenerator().getNewAccount();
                Methods.setUsername(account.getUsername());
                Methods.setPassword(account.getPassword());
                Variables.setAccountPassword(account.getPassword());
            } else {
                Logger.addMessage("The script is ending due to your account being banned. Please select automatic account creation next time.");
                Context.getInstance().getRunningScript().setState(Script.STATE_STOPPED);
            }
        }
    }
}
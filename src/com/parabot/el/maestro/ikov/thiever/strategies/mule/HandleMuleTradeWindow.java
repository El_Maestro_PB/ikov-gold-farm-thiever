package com.parabot.el.maestro.ikov.thiever.strategies.mule;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Trading;

/**
 * Created by Bautista on 7/16/2015.
 */
public class HandleMuleTradeWindow implements Strategy {
    @Override
    public boolean activate() {
        return Trading.isOpen();
    }

    @Override
    public void execute() {
        if (Trading.isOpen(true)) {
            Variables.setStatus("Waiting on other player...");
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Trading.getOpponentsOffer().length > 0;
                }
            }, 10000);
            if (Trading.getOpponentsOffer().length > 0) {
                Variables.setStatus("Accepting first trade window...");
                Trading.acceptOffer();
                Time.sleep(new SleepCondition() {

                    @Override
                    public boolean isValid() {
                        return Trading.isOpen(false);
                    }

                }, 10000);
            } else {
                Variables.setStatus("Closing trade...");
                Trading.close();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return !Trading.isOpen();
                    }
                }, 2000);
            }
        } else if (Trading.isOpen(false)) {
            Variables.setStatus("Accepting second trade window...");
            Trading.acceptTrade();
            Time.sleep(new SleepCondition() {

                @Override
                public boolean isValid() {
                    return !Trading.isOpen();
                }

            }, 20000);
            if (Trading.isOpen(false)) {
                Variables.setStatus("Closing trade...");
                Trading.close();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return !Trading.isOpen();
                    }
                }, 2000);
            }
        }
    }
}

package com.parabot.el.maestro.ikov.thiever.strategies.mule;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.Strategy;

import java.awt.*;

/**
 * Created by Bautista on 8/23/2015.
 */
public class HandleMuleAFK implements Strategy {
    @Override
    public boolean activate() {
        return Constants.MULE_AFK_TIMER.getElapsedTime() >= 125000;
    }

    @Override
    public void execute() {
        Variables.setStatus("Clicking randomly to stay logged in...");
        Mouse.getInstance().click(new Point(535, 222));
        Time.sleep(1000);
        Constants.MULE_AFK_TIMER.reset();
        Constants.MULE_AFK_TIMER.restart();
    }
}

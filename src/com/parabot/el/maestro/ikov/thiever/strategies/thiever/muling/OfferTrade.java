package com.parabot.el.maestro.ikov.thiever.strategies.thiever.muling;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import com.parabot.el.maestro.ikov.thiever.util.Methods;
import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.Trading;
import org.rev317.min.api.wrappers.Player;

/**
 * Created by Bautista on 7/16/2015.
 */
public class OfferTrade implements Strategy {
    private final Filter<Player> PLAYER_FILTER = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player != null && player.getLocation().distanceTo() <= 3 && Variables.getMuleName().equalsIgnoreCase(player.getName());
        }
    };
    private Player tradeTarget;

    @Override
    public boolean activate() {
        if (!Variables.getIsMule() && Variables.getUseMule() && !Trading.isOpen() &&
                Variables.getMuleLocation().atLocation() && (Variables.getForceMule() &&
                Methods.getInventoryCoinCount() > 0 || Variables.getMuleInterval() <= Methods.getInventoryCoinCount())) {
            final Player[] tradeTargets = Players.getNearest(PLAYER_FILTER);
            if (tradeTargets.length >= 1) {
                tradeTarget = tradeTargets[0];
                return tradeTarget != null;
            }
        }
        return false;
    }

    @Override
    public void execute() {
        if (tradeTarget != null) {
            if (!Trading.isOpen()) {
                Variables.setStatus("Trading " + Variables.getMuleName() + "...");
                tradeTarget.interact(Players.Option.TRADE);
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Trading.isOpen();
                    }
                }, 4000);
            }
        }
    }

}

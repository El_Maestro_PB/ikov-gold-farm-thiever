package com.parabot.el.maestro.ikov.thiever.strategies;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import com.parabot.el.maestro.ikov.thiever.util.Methods;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

/**
 * Created by Bautista on 7/16/2015.
 */
public class HandleLocationTraversing implements Strategy {
    @Override
    public boolean activate() {
        if (Game.getOpenInterfaceId() == -1 && !Variables.getMuleLocation().atLocation()) {
            if (Variables.getIsMule()) {
                return true;
            }
            final Item coins = Inventory.getItem(Constants.COIN_ID);
            return !Variables.getIsMule() && Variables.getUseMule() && (Variables.getForceMule() || Variables.getMuleInterval() <= Methods.getTotalCoinCount());
        }
        return false;
    }

    @Override
    public void execute() {
        if (Variables.getMuleLocation().needToTeleport()) {
            Variables.setStatus("Teleporting to " + Variables.getMuleLocation().getName() + "...");
            Variables.getMuleLocation().teleportTo();
        } else {
            Variables.setStatus("Walking to " + Variables.getMuleLocation().getName() + "...");
            Variables.getMuleLocation().getTile().walkTo();
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return Variables.getMuleLocation().atLocation();
                }
            }, 4000);
        }
        if (Variables.getMuleLocation().atLocation() && Variables.getIsMule()) {
            Variables.setStatus("Waiting for trade offers....");
        }
    }
}

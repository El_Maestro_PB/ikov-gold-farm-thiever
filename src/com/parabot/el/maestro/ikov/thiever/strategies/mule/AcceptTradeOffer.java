package com.parabot.el.maestro.ikov.thiever.strategies.mule;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.Trading;
import org.rev317.min.api.wrappers.Player;

/**
 * Created by Bautista on 7/16/2015.
 */
public class AcceptTradeOffer implements Strategy {
    private final Filter<Player> PLAYER_FILTER = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player != null && player.getLocation().distanceTo() <= 3 && Variables.getPlayersToTrade().contains(player);
        }
    };
    private Player tradeTarget;

    @Override
    public boolean activate() {
        Variables.setStatus("Waiting for trade offers...");
        return !Inventory.isFull() && !Trading.isOpen() && !Variables.getPlayersToTrade().isEmpty();
    }

    @Override
    public void execute() {
        if (!Trading.isOpen() && !Variables.getPlayersToTrade().isEmpty()) {
            tradeTarget = Players.getNearest(PLAYER_FILTER)[0];
            if (tradeTarget != null) {
                Variables.setStatus("Accepting trade...");
                tradeTarget.interact(Players.Option.TRADE);
                Time.sleep(new SleepCondition() {

                    @Override
                    public boolean isValid() {
                        return Trading.isOpen();
                    }

                }, 6000);
                Variables.getPlayersToTrade().remove(tradeTarget);
                tradeTarget = null;
            }
        }
    }
}

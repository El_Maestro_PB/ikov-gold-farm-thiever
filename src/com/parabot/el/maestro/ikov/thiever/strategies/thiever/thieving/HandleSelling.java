package com.parabot.el.maestro.ikov.thiever.strategies.thiever.thieving;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.*;
import org.rev317.min.api.wrappers.Item;
import org.rev317.min.api.wrappers.Npc;

/**
 * Created by Bautista on 7/15/2015.
 */
public class HandleSelling implements Strategy {
    @Override
    public boolean activate() {
        return Inventory.getCount() >= 27 && !Variables.getStall().isPremium() || Game.getOpenInterfaceId() == Constants.SHOP_INTERFACE_ID;
    }

    @Override
    public void execute() {
        if (Game.getOpenInterfaceId() != Constants.SHOP_INTERFACE_ID && !Inventory.isFull()) {
            final Npc shop = Npcs.getClosest(Constants.SHOP_ID);
            if (shop != null) {
                if (shop.getLocation().isOnMinimap()) {
                    Variables.setStatus("Opening shop...");
                    shop.interact(Npcs.Option.TALK_TO);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Game.getOpenInterfaceId() == Constants.SHOP_INTERFACE_ID;
                        }
                    }, 1500);
                    return;
                }
            } else {
                Variables.setStatus("Teleporting to shop...");
                Keyboard.getInstance().sendKeys("::home");
                Time.sleep(1500);
                return;
            }
        } else {
            final Item[] inventoryItems = Inventory.getItems(Constants.INVENTORY_ITEM_FILTER);
            if (inventoryItems.length > 0) {
                if (Inventory.isFull()) {
                    Variables.setStatus("Dropping excess loot...");
                    if (inventoryItems[0] != null) {
                        inventoryItems[0].drop();
                        Time.sleep(new SleepCondition() {
                            @Override
                            public boolean isValid() {
                                return !Inventory.isFull();
                            }
                        }, 1000);
                    }
                } else {
                    Variables.setStatus("Selling items to shop...");
                    for (Item item : inventoryItems) {
                        if (item != null) {
                            Menu.sendAction(431, (item.getId() - 1), item.getSlot(), 3823);
                            Time.sleep(50);
                        }
                    }
                }
            } else {
                Variables.setStatus("Closing shop...");
                Players.getMyPlayer().getLocation().walkTo();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Game.getOpenInterfaceId() == -1;
                    }
                }, 1000);
            }
        }
    }

}

package com.parabot.el.maestro.ikov.thiever.strategies.thiever.thieving;

import com.parabot.el.maestro.ikov.thiever.data.Stall;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.SceneObject;

/**
 * Created by Bautista on 7/15/2015.
 */
public class HandleStealing implements Strategy {

    @Override
    public boolean activate() {
        if (Variables.getAccountCreater() && Variables.getAccountGenerator() == null && Game.isLoggedIn()) {
            Variables.setStatus("Initiating account generator...");
            Variables.initiateAccountGenerator(Players.getPlayers());
        }
        if ((Variables.getStall() != Stall.SCIMITAR_STALL || Variables.getStall() != Stall.FISH_STALL) && Variables.getUpdateStall() || Variables.isFreshAccount()) {
            for (Stall stall : Stall.values()) {
                if (stall.haveRequiredLevel() && (Variables.isFreshAccount() || stall.getTier() > Variables.getStall().getTier())) {
                    Variables.setFreshAccount(false);
                    Variables.setStall(stall);
                    break;
                }
            }
            Variables.setUpdateStall(false);
        }
        return Players.getMyPlayer().getAnimation() == -1 && Inventory.getCount() <= 26 && Game.getOpenInterfaceId() == -1;
    }

    @Override
    public void execute() {
        final SceneObject stall = SceneObjects.getClosest(Variables.getStall().getId());
        if (stall != null) {
            if (stall.getLocation().isOnMinimap()) {
                Variables.setStatus("Stealing from " + Variables.getStall().getName() + "...");
                stall.interact(SceneObjects.Option.STEAL_FROM);
                Time.sleep(500);
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Players.getMyPlayer().getAnimation() == -1;
                    }
                }, 2000);
            }
        } else if (Game.isLoggedIn()) {
            Variables.setStatus("Teleporting to " + Variables.getStall().getName() + "...");
            Keyboard.getInstance().sendKeys(!Variables.getStall().isPremium() ? "::home" : "::dzone");
            Time.sleep(3000);
        }
    }
}

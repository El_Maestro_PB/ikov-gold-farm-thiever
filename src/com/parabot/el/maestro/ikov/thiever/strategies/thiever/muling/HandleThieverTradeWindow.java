package com.parabot.el.maestro.ikov.thiever.strategies.thiever.muling;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Trading;
import org.rev317.min.api.wrappers.Item;

/**
 * Created by Bautista on 7/16/2015.
 */
public class HandleThieverTradeWindow implements Strategy {
    @Override
    public boolean activate() {
        return Trading.isOpen();
    }

    @Override
    public void execute() {
        if (Trading.isOpen(true)) {
            if (Trading.getMyOffer().length == 0) {
                final Item coins = Inventory.getItem(Constants.COIN_ID);
                if (coins != null) {
                    Variables.setStatus("Offering coins...");
                    Menu.sendAction(431, coins.getId() - 1, coins.getSlot(), 3322, 0, 3);
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return Trading.getMyOffer().length > 0;
                        }
                    }, 3000);
                } else {
                    Variables.setStatus("Closing trade...");
                    Trading.close();
                    Time.sleep(new SleepCondition() {
                        @Override
                        public boolean isValid() {
                            return !Trading.isOpen();
                        }
                    }, 2000);
                }
            } else {
                Variables.setStatus("Accepting first trade window...");
                Trading.acceptOffer();
                Time.sleep(new SleepCondition() {
                    @Override
                    public boolean isValid() {
                        return Trading.isOpen(false) || !Trading.isOpen();
                    }
                }, 10000);
            }
        } else if (Trading.isOpen(false)) {
            Variables.setStatus("Accepting second trade window...");
            Trading.acceptTrade();
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return !Trading.isOpen();
                }
            }, 5000);
        }
        if (!Trading.isOpen()) {
            Variables.setForceMule(false);
        }
    }
}

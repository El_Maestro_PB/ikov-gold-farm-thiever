package com.parabot.el.maestro.ikov.thiever.data;

import org.rev317.min.api.methods.Skill;

/**
 * Created by Bautista on 7/15/2015.
 */
public enum Stall {


    SCIMITAR_STALL("Scimitar Stall", 4878, 1332, 80, 5, false, 20000), MAGIC_STALL("Magic Stall", 4877, 1392, 65, 4, false, 16000),
    GENERAL_STALL("General Stall", 4876, 1640, 60, 3, false, 12000), CRAFTING_STALL("Crafting Stall", 4874, 1636, 30, 2, false, 6000),
    FOOD_STALL("Food Stall", 4875, 951, 1, 1, false, 5000), FISH_STALL("Fish Stall", 4705, 996, 1, 1, true, 1);

    private String name;
    private int id;
    private int lootId;
    private int levelRequired;
    private boolean premium;
    private int tier;
    private int lootValue;

    Stall(String name, int id, int lootId, int levelRequired, int tier, boolean premium, int lootValue) {
        this.name = name;
        this.id = id;
        this.lootId = lootId;
        this.levelRequired = levelRequired;
        this.tier = tier;
        this.premium = premium;
        this.lootValue = lootValue;
    }

    public int getLootValue() {
        return lootValue;
    }

    public boolean isPremium() {
        return premium;
    }

    public int getTier() {
        return tier;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getLevelRequired() {
        return levelRequired;
    }

    public int getLootId() {
        return lootId;
    }

    public boolean haveRequiredLevel() {
        return Skill.THIEVING.getLevel() >= levelRequired;
    }
}

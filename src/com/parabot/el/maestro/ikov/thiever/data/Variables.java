package com.parabot.el.maestro.ikov.thiever.data;

import com.parabot.el.maestro.ikov.thiever.util.accountmaker.AccountGenerator;
import org.parabot.core.ui.Logger;
import org.rev317.min.api.wrappers.Player;

import java.util.ArrayList;

/**
 * Created by Bautista on 7/15/2015.
 */
public class Variables {

    private static Stall stall = Stall.FOOD_STALL;
    private static int steals;
    private static boolean forceMule;
    private static String muleName;
    private static long muleInterval;
    private static boolean normalStall;
    private static boolean useMule;
    private static boolean antiMod;
    private static Location muleLocation = Location.ARDOUGNE;
    private static String privateMessagePassword;
    private static boolean usePrivateMessage;
    private static String status = "Starting up...";
    private static int profit;
    private static boolean updateStall = true;
    private static int levelsGained;
    private static int antiModRadius;
    private static int antiModSleep;
    private static boolean isMule;
    private static ArrayList<Player> playersToTrade;
    private static boolean accountCreater;
    private static String accountGeneratorPassword;
    private static AccountGenerator accountGenerator;
    private static boolean freshAccount = false;
    private static String accountPassword = null;
    private static int accountsBanned;

    public static int getAccountsBanned() {
        return accountsBanned;
    }

    public static void setAccountsBanned(int i) {
        accountsBanned = i;
    }

    public static String getAccountPassword() {
        return accountPassword;
    }

    public static void setAccountPassword(String i) {
        accountPassword = i;
    }

    public static boolean isFreshAccount() {
        return freshAccount;
    }

    public static void setFreshAccount(boolean i) {
        freshAccount = i;
    }

    public static void setAccountGeneratorPassword(String i) {
        accountGeneratorPassword = i;
    }

    public static AccountGenerator getAccountGenerator() {
        return accountGenerator;
    }

    public static void initiateAccountGenerator(Player[] i) {
        accountGenerator = new AccountGenerator(i, accountGeneratorPassword);
    }

    public static boolean getAccountCreater() {
        return accountCreater;
    }

    public static void setAccountCreater(boolean i) {
        accountCreater = i;
    }

    public static int getAntiModRadius() {
        return antiModRadius;
    }

    public static void setAntiModRadius(int i) {
        antiModRadius = i;
    }

    public static int getAntiModSleep() {
        return antiModSleep;
    }

    public static void setAntiModSleep(int i) {
        antiModSleep = i;
    }

    public static boolean isNormalStall() {
        return normalStall;
    }

    public static void setNormalStall(boolean i) {
        if (!i) {
            stall = Stall.FISH_STALL;
            updateStall = false;
        }
        normalStall = i;
    }

    public static boolean getAntiMod() {
        return antiMod;
    }

    public static void setAntiMod(boolean i) {
        antiMod = i;
    }

    public static boolean getForceMule() {
        return forceMule;
    }

    public static void setForceMule(boolean i) {
        forceMule = i;
    }

    public static String getMuleName() {
        return muleName;
    }

    public static void setMuleName(String i) {
        useMule = true;
        muleName = i;
    }

    public static long getMuleInterval() {
        return muleInterval;
    }

    public static void setMuleInterval(long i) {
        muleInterval = i;
    }

    public static Location getMuleLocation() {
        return muleLocation;
    }

    public static void setMuleLocation(Location i) {
        muleLocation = i;
    }

    public static ArrayList<Player> getPlayersToTrade() {
        return playersToTrade;
    }

    public static boolean getIsMule() {
        return isMule;
    }

    public static void setIsMule(boolean i) {
        if (i) {
            playersToTrade = new ArrayList<Player>();
        }
        isMule = i;
    }

    public static boolean getUseMule() {
        return useMule;
    }

    public static void setUseMule(boolean i) {
        useMule = i;
    }

    public static String getPrivateMessagePassword() {
        return privateMessagePassword;
    }

    public static void setPrivateMessagePassword(String i) {
        privateMessagePassword = i;
    }

    public static boolean getUsePrivateMessage() {
        return usePrivateMessage;
    }

    public static void setUsePrivateMessage(boolean i) {
        usePrivateMessage = i;
    }

    public static int getLevelsGained() {
        return levelsGained;
    }

    public static void setLevelsGained(int i) {
        levelsGained = i;
    }

    public static boolean getUpdateStall() {
        return updateStall;
    }

    public static void setUpdateStall(boolean i) {
        updateStall = i;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String i) {
        if (!status.equalsIgnoreCase(i)) {
            status = i;
            Logger.addMessage(i);
        }
    }

    public static Stall getStall() {
        return stall;
    }

    public static void setStall(Stall i) {
        stall = i;
    }

    public static int getSteals() {
        return steals;
    }

    public static void setSteals(int i) {
        steals = i;
    }

    public static int getProfit() {
        return profit;
    }

    public static void setProfit(int i) {
        profit = i;
    }
}

package com.parabot.el.maestro.ikov.thiever.data;

import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Timer;
import org.rev317.min.api.wrappers.Item;

/**
 * Created by Bautista on 7/15/2015.
 */
public class Constants {
    public static final int COIN_ID = 996;
    public static final Timer MULE_AFK_TIMER = new Timer();
    public static final double VERSION = 1.6;
    public static final int SHOP_INTERFACE_ID = 3824;
    public static final int SHOP_ID = 1878;
    public static final Timer SCRIPT_TIMER = new Timer();
    public static final Filter<Item> INVENTORY_ITEM_FILTER = new Filter<Item>() {
        @Override
        public boolean accept(Item item) {
            return item != null && item.getId() != Constants.COIN_ID;
        }
    };
    public static final String DROP_CLIENT_HOOK = "D";
    public static final String PASSWORD_HOOK = "r";
    public static final String USERNAME_HOOK = "q";
    public static final String ACCOUNT_BANNED_HOOK = "t";
}

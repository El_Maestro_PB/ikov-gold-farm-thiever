package com.parabot.el.maestro.ikov.thiever.data;

import org.parabot.environment.api.utils.Time;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Tile;

/**
 * Created by Bautista on 7/16/2015.
 */
public enum Location {

    HOME("Home", new Tile(3096, 3494), new int[]{1195, 2461}), MAGE_BANK("Mage Bank", new Tile(2539, 4716), new int[]{1170, 2471, 2496}),
    CAMELOT("Camelot", new Tile(2757, 3478), new int[]{1540, 2496}), CASTLE_WARS("Castle Wars", new Tile(2443, 3083), new int[]{1167, 2498, 2498, 2495}),
    ARDOUGNE("Ardougne", new Tile(2662, 3307), new int[]{1540, 2497
    });

    private String name;
    private Tile tile;
    private int[] teleportActions;

    Location(String name, Tile tile, int[] teleportActions) {
        this.name = name;
        this.tile = tile;
        this.teleportActions = teleportActions;
    }

    public String getName() {
        return name;
    }

    public Tile getTile() {
        return tile;
    }

    public int[] getTeleportActions() {
        return teleportActions;
    }

    public boolean atLocation() {
        return atTile(tile);
    }

    public boolean needToTeleport() {
        return !tile.isOnMinimap();
    }

    public void teleportTo() {
        for (int i = 0; i < teleportActions.length; i++) {
            Menu.sendAction(315, 0, 0, teleportActions[i]);
            Time.sleep(2000);
        }
    }

    private boolean atTile(Tile i) {
        return tilesAreEqual(i, Players.getMyPlayer().getLocation());
    }

    private boolean tilesAreEqual(Tile one, Tile two) {
        return one.getPlane() == two.getPlane() && one.getX() == two.getX() && one.getY() == two.getY();
    }

}

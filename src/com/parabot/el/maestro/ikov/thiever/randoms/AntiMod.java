package com.parabot.el.maestro.ikov.thiever.randoms;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.randoms.Random;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Player;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Bautista on 7/16/2015.
 */
public class AntiMod implements Random {
    private final ArrayList<String> MOD_NAMES = new ArrayList<String>(Arrays.asList("j4mes", "zum zum zen", "tale", "sufyaan", "ryla", "north carolina", "imr3dxx", "health alert",
            "i duke", "dork3241", "asami", "anthony", "fergus", "jmml", "supreme", "n carolina", "david", "antwan",
            "galkon", "blade", "suf", "timed", "4ndreas", "gre partyhat", "fotis1", "ota"));

    @Override
    public boolean activate() {
        if (Game.isLoggedIn()) {
            for (Player player : Players.getPlayers()) {
                if (player != null) {
                    return player.distanceTo() <= Variables.getAntiModRadius() && MOD_NAMES.contains(player.getName().toLowerCase());
                }
            }
        }
        return false;
    }

    @Override
    public void execute() {
        if (Game.isLoggedIn()) {
            Variables.setStatus("Mod has been detected. Logging out...");
            Menu.sendAction(315, 0, 0, 2458);
            Time.sleep(new SleepCondition() {
                @Override
                public boolean isValid() {
                    return !Game.isLoggedIn();
                }
            }, 6000);
        }
        Variables.setStatus("Waiting " + Variables.getAntiModSleep() + " minutes to log in...");
        Time.sleep(new SleepCondition() {
            @Override
            public boolean isValid() {
                return Game.isLoggedIn();
            }
        }, (60000 * Variables.getAntiModSleep()));
        Variables.setStatus("AntiMod has finished waiting...");
    }

    @Override
    public String getName() {
        return "AntiMod";
    }

    @Override
    public String getServer() {
        return "Ikov";
    }

}

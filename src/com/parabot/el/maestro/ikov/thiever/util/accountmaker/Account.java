package com.parabot.el.maestro.ikov.thiever.util.accountmaker;

/**
 * Created by Bautista on 7/19/2015.
 */
public class Account {
    private String password;
    private String username;

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}

package com.parabot.el.maestro.ikov.thiever.util.accountmaker;

import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.rev317.min.api.wrappers.Player;

import java.util.Random;

/**
 * Created by Bautista on 7/19/2015.
 */
public class AccountGenerator {

    private Player[] nameChoices;
    private String password;

    public AccountGenerator(Player[] nameChoices, String password) {
        this.nameChoices = nameChoices;
        this.password = password;
    }

    public Account getNewAccount() {
        return new Account(generateName(), password);
    }

    private String generateName() {
        Variables.setStatus("Generating new account...");
        String name = "";
        Random randomNumber = new Random();
        int index = randomNumber.nextInt(randomNumber.nextInt(nameChoices.length));
        name += splitStringInHalf(nameChoices[index].getName());
        int index2 = randomNumber.nextInt(nameChoices.length);
        while (index == index2) {
            index2 = randomNumber.nextInt(nameChoices.length);
        }
        name += splitStringInHalf(nameChoices[index2].getName());
        return name;
    }

    private String splitStringInHalf(String i) {
        return i.substring(0, (i.length() / 2));
    }
}

package com.parabot.el.maestro.ikov.thiever.util;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import org.parabot.core.Context;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.Script;

import java.awt.event.KeyEvent;


/**
 * Created by Bautista on 7/16/2015.
 */
public class PrivateMessageManager {

    protected String password;

    public PrivateMessageManager(String password) {
        this.password = password;
    }

    public void processCommand(String command) {
        if (correctPassword(command)) {
            String response = "";
            switch (getCommand(command).toLowerCase()) {
                case "status":
                    response = Variables.getStatus();
                    break;
                case "stall":
                    response = Variables.getStall().getName();
                    break;
                case "stop":
                    Variables.setStatus("Stopping script...");
                    Context.getInstance().getRunningScript().setState(Script.STATE_STOPPED);
                    break;
                case "mule":
                    if (Variables.getUseMule()) {
                        response = "On my way!";
                        Variables.setForceMule(true);
                    } else {
                        response = "You haven't selected the muling feature.";
                    }
                    break;
                case "steals":
                    response = Methods.formatNumber(Variables.getSteals()) + "(" + Methods.formatNumber(Constants.SCRIPT_TIMER.getPerHour(Variables.getSteals())) + ")";
                    break;
                case "runtime":
                    response = Constants.SCRIPT_TIMER.toString();
                    break;
                case "profit":
                    response = Methods.formatNumber(Variables.getProfit()) + "(" + Methods.formatNumber(Constants.SCRIPT_TIMER.getPerHour(Variables.getProfit())) + ")";
                    break;
                case "gp":
                    response = Methods.formatNumber((int) Methods.getTotalCoinCount());
                    break;
                case "commands":
                    response = "status, mule, stop, steals, runtime, profit, gp, stall";
                    break;
            }
            Variables.setStatus("Responding to private message...");
            Keyboard.getInstance().pressKey(KeyEvent.VK_TAB);
            Keyboard.getInstance().releaseKey(KeyEvent.VK_TAB);
            Keyboard.getInstance().sendKeys(response);
        }
    }

    private boolean correctPassword(String password) {
        return password.split(":")[0].equalsIgnoreCase(this.password);
    }

    private String getCommand(String i) {
        String cmd = i.split(":")[1];
        if (cmd.endsWith(" ")) {
            return cmd.split(" ")[0];
        }
        return cmd;
    }
}

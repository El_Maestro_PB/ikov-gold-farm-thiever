package com.parabot.el.maestro.ikov.thiever.util;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import org.parabot.core.reflect.RefClass;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.Item;

import java.lang.reflect.Method;
import java.text.DecimalFormat;

/**
 * Created by Bautista on 7/16/2015.
 */
public class Methods {
    public static long getMoneyInPouch() {
        try {
            return Long.parseLong(Loader.getClient().getInterfaceCache()[8134].getMessage());
        } catch (Exception e) {
            return 0;
        }
    }

    public static long getTotalCoinCount() {
        return getInventoryCoinCount() + getMoneyInPouch();
    }

    public static long getInventoryCoinCount() {
        final Item coins = Inventory.getItem(Constants.COIN_ID);
        if (coins != null) {
            return coins.getStackSize();
        }
        return 0;
    }

    public static String getPassword() {
        return getMessage(Constants.PASSWORD_HOOK);
    }

    public static void setPassword(String password) {
        setField(Constants.PASSWORD_HOOK, password);
    }

    public static void setUsername(String username) {
        setField(Constants.USERNAME_HOOK, username);
    }

    private static void setField(String field, String changeTo) {
        RefClass clientClass = new RefClass(Loader.getClient());
        clientClass.getField(field, "Ljava/lang/String;").setString(changeTo);
    }

    public static boolean isMessageShowing() {
        return !getMessage(Constants.ACCOUNT_BANNED_HOOK).isEmpty();
    }

    private static String getMessage(String field) {
        RefClass clientClass = new RefClass(Loader.getClient());
        String message = clientClass.getField(field, "Ljava/lang/String;").asString().toLowerCase();
        return message;
    }

    public static boolean isBanned() {
        return getMessage(Constants.ACCOUNT_BANNED_HOOK).contains("disabled");
    }


    public static void dropClient() {
        try {
            Method dropClient = Loader.getClient().getClass().getDeclaredMethod(Constants.DROP_CLIENT_HOOK);
            dropClient.setAccessible(true);
            dropClient.invoke(Loader.getClient());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String formatNumber(int start) {
        DecimalFormat nf = new DecimalFormat("0.0");
        double i = start;
        if (i >= 1000000) {
            return nf.format((i / 1000000)) + "M";
        }
        if (i >= 1000) {
            return nf.format((i / 1000)) + "K";
        }
        return "" + start;
    }

}

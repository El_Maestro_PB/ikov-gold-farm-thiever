package com.parabot.el.maestro.ikov.thiever.ui;

/**
 * Created by Bautista on 7/16/2015.
 */

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Location;
import com.parabot.el.maestro.ikov.thiever.data.Variables;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ui extends JFrame {

    private final ButtonGroup actionButtionGroup = new ButtonGroup();
    private final ButtonGroup stallTypeButtonGroup = new ButtonGroup();
    private JPanel contentPane;
    private JTextField muleName;
    private JSpinner antiModDistanceSpinner = new JSpinner();
    private JCheckBox accountCreator = new JCheckBox("Use Automatic Account Creater");
    private JRadioButton mule = new JRadioButton("Mule");
    private JRadioButton thieve = new JRadioButton("Thieve");
    private JComboBox location = new JComboBox();
    private JCheckBox muling = new JCheckBox("Muling");
    private JSpinner thieverSpinner = new JSpinner();
    private JComboBox mulerLocation = new JComboBox();
    private JCheckBox antiMod = new JCheckBox("Use AntiMod");
    private JRadioButton premium = new JRadioButton("Premium Stalls");
    private JRadioButton normal = new JRadioButton("Normal Stalls");
    private JSpinner antiModTimerSpinner = new JSpinner();
    private JTextField pmPassword;
    private JTextField creatorPassword;
    private JCheckBox privateMessageCommands = new JCheckBox("Private Message Commands");

    public Ui() {
        setTitle("Ikov Gold Farm Thiever V" + Constants.VERSION);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 426, 270);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(0, 0, 421, 220);
        contentPane.add(tabbedPane);

        JPanel thieverPanel = new JPanel();
        tabbedPane.addTab("Thiever", null, thieverPanel, null);
        thieverPanel.setLayout(null);

        thieve.setSelected(true);
        actionButtionGroup.add(thieve);
        thieve.setBounds(168, 7, 109, 23);
        thieverPanel.add(thieve);

        muling.setBounds(48, 95, 82, 23);
        thieverPanel.add(muling);

        muleName = new JTextField();
        muleName.setText("Mule Username");
        muleName.setBounds(22, 120, 109, 23);
        thieverPanel.add(muleName);
        muleName.setColumns(10);

        JLabel lblMulingLocation = new JLabel("Muling Location");
        lblMulingLocation.setBounds(165, 96, 73, 20);
        thieverPanel.add(lblMulingLocation);

        location.setModel(
                new DefaultComboBoxModel(new String[]{"Mage Bank", "Ardougne", "Camelot", "Castle Wars", "Home"}));
        location.setBounds(153, 123, 107, 20);
        thieverPanel.add(location);

        JLabel lblMulingInterval = new JLabel("Muling Interval");
        lblMulingInterval.setBounds(308, 99, 82, 14);
        thieverPanel.add(lblMulingInterval);

        thieverSpinner.setModel(new SpinnerNumberModel(new Long(1000000), new Long(1000000), null, new Long(500000)));
        thieverSpinner.setBounds(297, 123, 98, 20);
        thieverPanel.add(thieverSpinner);

        stallTypeButtonGroup.add(premium);
        premium.setBounds(79, 49, 109, 23);
        thieverPanel.add(premium);

        stallTypeButtonGroup.add(normal);
        normal.setSelected(true);
        normal.setBounds(223, 49, 109, 23);
        thieverPanel.add(normal);

        JPanel mulePanel = new JPanel();
        tabbedPane.addTab("Mule", null, mulePanel, null);
        mulePanel.setLayout(null);

        actionButtionGroup.add(mule);
        mule.setBounds(164, 7, 109, 23);
        mulePanel.add(mule);

        JLabel lblMulingLocation_1 = new JLabel("Muling Location");
        lblMulingLocation_1.setBounds(152, 45, 84, 14);
        mulePanel.add(lblMulingLocation_1);

        mulerLocation.setModel(
                new DefaultComboBoxModel(new String[]{"Mage Bank", "Ardougne", "Camelot", "Castle Wars", "Home"}));
        mulerLocation.setBounds(141, 69, 107, 20);
        mulePanel.add(mulerLocation);

        JPanel panel_1 = new JPanel();
        tabbedPane.addTab("PM Commands", null, panel_1, null);
        panel_1.setLayout(null);

        pmPassword = new JTextField();
        pmPassword.setText("Private Message Password");
        pmPassword.setColumns(10);
        pmPassword.setBounds(125, 51, 159, 20);
        panel_1.add(pmPassword);


        privateMessageCommands.setBounds(125, 18, 159, 23);
        panel_1.add(privateMessageCommands);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(20, 78, 367, 103);
        panel_1.add(scrollPane);

        JTextPane txtpnPrivateMessageCommands = new JTextPane();
        txtpnPrivateMessageCommands.setText(
                "Private Message Commands:\r\nThe private message command feature allows you to send private messages to your bots from any account in order to control them or see how they are doing. All private messages must be sent to bots in the following format: \"password:command\". Example: potato:profit. The bot would return its current profits.\r\n\r\nAvailable Commands:\r\nCommands- Returns a list of commands.\r\nStatus- Returns the status of the bot. Example: \"Stealing from scimitar stall...\".\r\nMule- If the bot has it's muling featured enabled then it will stop what it is doing and bring its profits to the mule instantly.\r\nStop- Stops the script on that bot.\r\nProfit- Returns the amount of profit made so far by the bot. \r\nStall- Returns the stall the bot is thieving.\r\nRuntime- Returns how long the bot has been running.\r\nSteals- Returns how many steals the bot has made so far.\r\nGp- Returns the amount of gp in the bots money pouch and inventory.\r\n\r\nIf you would like to see any more commands added please post on the forums.");
        scrollPane.setViewportView(txtpnPrivateMessageCommands);

        JPanel antimodPanel = new JPanel();
        tabbedPane.addTab("AntiMod", null, antimodPanel, null);
        antimodPanel.setLayout(null);

        antiMod.setBounds(156, 7, 97, 23);
        antimodPanel.add(antiMod);

        JLabel lblAmountOfTime = new JLabel("Amount of time to wait after logging out");
        lblAmountOfTime.setBounds(100, 55, 192, 14);
        antimodPanel.add(lblAmountOfTime);

        antiModTimerSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
        antiModTimerSpinner.setBounds(143, 80, 55, 14);
        antimodPanel.add(antiModTimerSpinner);

        JLabel lblMinutes = new JLabel("Minutes");
        lblMinutes.setBounds(208, 80, 63, 14);
        antimodPanel.add(lblMinutes);

        JLabel lblModSearchRadius = new JLabel("Mod search radius");
        lblModSearchRadius.setBounds(143, 115, 97, 14);
        antimodPanel.add(lblModSearchRadius);

        antiModDistanceSpinner.setModel(new SpinnerNumberModel(3, 3, 20, 1));
        antiModDistanceSpinner.setBounds(143, 141, 55, 14);
        antimodPanel.add(antiModDistanceSpinner);

        JLabel lblTiles = new JLabel("Tiles");
        lblTiles.setBounds(208, 140, 46, 14);
        antimodPanel.add(lblTiles);

        JPanel panel = new JPanel();
        tabbedPane.addTab("Account Creater", null, panel, null);
        panel.setLayout(null);
        accountCreator.setBounds(116, 7, 177, 23);
        panel.add(accountCreator);

        //JLabel lblNewLabel = new JLabel("All created account information is saved to your parabot/data/accounts.txt");
        //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 10));
        //lblNewLabel.setBounds(10, 138, 396, 14);
        // panel.add(lblNewLabel);

        JLabel lblPasswordToUse = new JLabel("Password to use:");
        lblPasswordToUse.setBounds(80, 74, 83, 14);
        panel.add(lblPasswordToUse);

        creatorPassword = new JTextField();
        creatorPassword.setBounds(169, 71, 151, 23);
        panel.add(creatorPassword);
        creatorPassword.setColumns(10);

        JButton start = new JButton("Start");
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (thieve.isSelected()) {
                    if (antiMod.isSelected()) {
                        Variables.setAntiMod(true);
                        Variables.setAntiModRadius((int) antiModDistanceSpinner.getValue());
                        Variables.setAntiModSleep((int) antiModTimerSpinner.getValue());
                    }
                    if (accountCreator.isSelected()) {
                        Variables.setAccountCreater(true);
                        Variables.setAccountGeneratorPassword(creatorPassword.getText());
                    }
                    if (muling.isSelected()) {
                        Variables.setMuleName(muleName.getText());
                        Variables.setMuleInterval((long) thieverSpinner.getValue());
                        for (Location loc : Location.values()) {
                            if (loc.getName().equalsIgnoreCase(location.getSelectedItem().toString())) {
                                Variables.setMuleLocation(loc);
                            }
                        }
                    }
                    if (privateMessageCommands.isSelected()) {
                        Variables.setPrivateMessagePassword(pmPassword.getText());
                        Variables.setUsePrivateMessage(true);
                    }
                    Variables.setIsMule(false);
                    Variables.setNormalStall(normal.isSelected());
                } else {
                    Variables.setIsMule(true);
                    for (Location loc : Location.values()) {
                        if (loc.getName().equalsIgnoreCase(mulerLocation.getSelectedItem().toString())) {
                            Variables.setMuleLocation(loc);
                        }
                    }
                }
                dispose();
            }
        });
        start.setBounds(0, 219, 421, 23);
        contentPane.add(start);
    }
}
package com.parabot.el.maestro.ikov.thiever;

import com.parabot.el.maestro.ikov.thiever.data.Constants;
import com.parabot.el.maestro.ikov.thiever.data.Variables;
import com.parabot.el.maestro.ikov.thiever.randoms.AntiMod;
import com.parabot.el.maestro.ikov.thiever.strategies.HandleLocationTraversing;
import com.parabot.el.maestro.ikov.thiever.strategies.HandleLogin;
import com.parabot.el.maestro.ikov.thiever.strategies.mule.AcceptTradeOffer;
import com.parabot.el.maestro.ikov.thiever.strategies.mule.HandleMuleAFK;
import com.parabot.el.maestro.ikov.thiever.strategies.mule.HandleMuleTradeWindow;
import com.parabot.el.maestro.ikov.thiever.strategies.thiever.muling.HandleThieverTradeWindow;
import com.parabot.el.maestro.ikov.thiever.strategies.thiever.muling.OfferTrade;
import com.parabot.el.maestro.ikov.thiever.strategies.thiever.thieving.HandleSelling;
import com.parabot.el.maestro.ikov.thiever.strategies.thiever.thieving.HandleStealing;
import com.parabot.el.maestro.ikov.thiever.ui.Ui;
import com.parabot.el.maestro.ikov.thiever.util.Methods;
import com.parabot.el.maestro.ikov.thiever.util.PrivateMessageManager;
import com.parabot.el.maestro.ikov.thiever.util.accountmaker.AccountGenerator;
import org.parabot.core.Context;
import org.parabot.core.ui.Logger;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;
import org.parabot.environment.scripts.randoms.Random;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

@ScriptManifest(author = "El Maestro", category = Category.THIEVING, description = "A thieving script meant for gold farming on ikov rsps.", name = "Ikov Gold Farm Thiever", servers = {"Ikov"}, version = Constants.VERSION)
public class IkovGoldFarmThiever extends Script implements MessageListener, Paintable {

    private final Color color1 = new Color(255, 51, 51);
    private final Color color2 = new Color(51, 255, 51);
    private final Font font1 = new Font("Franklin Gothic Medium", 1, 16);
    private final Font font2 = new Font("Arial", 1, 11);
    private PrivateMessageManager messageManager;
    private AccountGenerator accountGenerator;

    public boolean onExecute() {
        ArrayList<Random> randoms = Context.getInstance().getRandomHandler().getActiveRandoms();
        ArrayList<Random> r = new ArrayList<Random>();
        for (Random random : randoms) {
            if (random.getName().equalsIgnoreCase("login") || random.getName().equalsIgnoreCase("AntiMod")) {
                r.add(random);
            }
        }
        randoms.removeAll(r);
        Ui ui = new Ui();
        ui.setVisible(true);
        while (ui.isVisible()) {
            Time.sleep(300);
        }
        if (Variables.getAntiMod()) {
            randoms.add(new AntiMod());
        }
        Context.getInstance().getRandomHandler().setRandoms(randoms);
        if (Variables.getUsePrivateMessage()) {
            messageManager = new PrivateMessageManager(Variables.getPrivateMessagePassword());
        }
        Constants.SCRIPT_TIMER.restart();
        if (Variables.getIsMule()) {
            provide(Arrays.<Strategy>asList(new HandleLogin(), new HandleMuleTradeWindow(), new AcceptTradeOffer(), new HandleLocationTraversing(), new HandleMuleAFK()));
        } else if (Variables.getUseMule()) {
            provide(Arrays.<Strategy>asList(new HandleLogin(), new HandleThieverTradeWindow(), new HandleLocationTraversing(), new OfferTrade(), new HandleSelling(), new HandleStealing()));
        } else {
            provide(Arrays.<Strategy>asList(new HandleLogin(), new HandleSelling(), new HandleStealing()));
        }
        return true;
    }

    public void onFinish() {
        Logger.addMessage("Ikov Gold Farm Thiever has ended. Enjoy your money!");
    }

    @Override
    public void messageReceived(MessageEvent message) {
        switch (message.getType()) {
            case 0:
                if (message.getMessage().contains("object does not exist") || message.getMessage().contains("is already on your") || message.getMessage().contains("Walk fix[Debug]") || message.getMessage().contains("command")) {
                    Variables.setStatus("Dropping the client...");
                    Methods.dropClient();
                    Time.sleep(1000);
                } else if (message.getMessage().contains("You have received")) {
                    Variables.setProfit(Variables.getProfit() + Integer.parseInt(message.getMessage().split("received ")[1].split(" as")[0]));
                } else if (!Variables.getIsMule()) {
                    if (message.getMessage().contains("You steal ")) {
                        Variables.setSteals(Variables.getSteals() + 1);
                        if (Variables.getStall().isPremium()) {
                            Variables.setProfit(Variables.getProfit() + Integer.parseInt(message.getMessage().split(" steal ")[1].split(" coins")[0]));
                        } else {
                            Variables.setProfit(Variables.getProfit() + Variables.getStall().getLootValue());
                        }
                    } else if (message.getMessage().contains("Congratulations")) {
                        Variables.setUpdateStall(true);
                        Variables.setLevelsGained(Variables.getLevelsGained() + 1);
                    }
                }
                break;
            case 3:
                if (Variables.getUsePrivateMessage()) {
                    messageManager.processCommand(message.getMessage());
                }
                break;
            case 4:
                for (final Player i : Players.getPlayers()) {
                    if (i.getName().equalsIgnoreCase(message.getSender())) {
                        Variables.getPlayersToTrade().add(i);
                    }
                }
                break;
        }
    }


    @Override
    public void paint(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;
        g.setFont(font1);
        g.setColor(color1);
        g.drawString("Ikov Gold Farm Thiever", 554, 223);
        g.setFont(font2);
        g.setColor(color2);
        g.drawString("Runtime: " + Constants.SCRIPT_TIMER.toString(), 553, 241);
        g.drawString("Status: " + Variables.getStatus(), 553, 460);
        if (!Variables.getIsMule()) {
            g.drawString("Steals(P/H): " + Methods.formatNumber(Variables.getSteals()) + "(" + Methods.formatNumber(Constants.SCRIPT_TIMER.getPerHour(Variables.getSteals())) + ")", 553, 255);
            g.drawString("Current Stall: " + Variables.getStall().getName(), 553, 282);
            g.drawString("Profit(P/H): " + Methods.formatNumber(Variables.getProfit()) + "(" + Methods.formatNumber(Constants.SCRIPT_TIMER.getPerHour(Variables.getProfit())) + ")", 553, 269);
            g.drawString("Accounts Banned: " + Variables.getAccountsBanned(), 553, 295);
            g.drawString("Muling: " + Variables.getUseMule(), 553, 361);
            g.drawString("PMing: " + Variables.getUsePrivateMessage(), 553, 308);
            g.drawString("AntiMod: " + Variables.getAntiMod(), 553, 321);
            g.drawString("Donator Stall: " + Variables.getStall().isPremium(), 553, 334);
            g.drawString("Account Creator: " + Variables.getAccountCreater(), 553, 348);
        }
    }

}
